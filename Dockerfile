FROM node:9.5-alpine

RUN mkdir -p /usr/src/app/dist
WORKDIR /usr/src/app
ENV NODE_ENV production

ADD yarn.lock /usr/src/app/
ADD package.json /usr/src/app/

RUN yarn install

COPY dist /usr/src/app/dist/

CMD ["node", "./dist/bin/www.js"]