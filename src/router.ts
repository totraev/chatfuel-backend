import { Router } from 'express';
import * as userController from './controllers/user';

const router = Router();

router
  .get('/users', userController.index)
  .get('/users/:id', userController.view);

export default router;
