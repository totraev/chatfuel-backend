import * as querystring from 'querystring';
import User from '../models/user';


export type UserParams = {
  page?: string
  limit?: string
  searchTerm: string
};

const FIRST_PAGE = 1;
const PAGE_LIMIT = 50;


class UserService {
  private page: number;
  private limit: number;
  private searchTerm: string;
  private pages: number;

  constructor(params: UserParams) {
    const page = parseInt(params.page, 10);
    const limit = parseInt(params.limit, 10);
  
    this.searchTerm = params.searchTerm;
    this.page = page > 0 ? page : FIRST_PAGE;
    this.limit = limit > 0 ? limit : PAGE_LIMIT;
  }

  private get options() {
    return { 
      limit: this.limit,
      page: this.page
    };
  }

  private get query() {
    return this.searchTerm
      ? { name: new RegExp(`^${this.searchTerm}`, 'i') }
      : {};
  }

  private getUrl(page: number) {
    const query = querystring.stringify({ 
      page,
      limit: this.limit,
      searchTerm: this.searchTerm
    });
    
    return `/api/users?${query}`;
  }

  private get nextPageUrl() {
    return this.page < this.pages ? this.getUrl(this.page + 1) : undefined;
  }

  private get previousPageUrl() {
    return this.page > 1 ? this.getUrl(this.page - 1) : undefined;
  }

  findUsers = async () => {
    const { docs, pages } = await User.paginate(this.query, this.options);

    this.pages = pages;

    return {
      result: docs,
      nextPageUrl: this.nextPageUrl,
      previousPageUrl: this.previousPageUrl
    };
  }
}


export default UserService;
