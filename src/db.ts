import * as mongoose from 'mongoose';
import config from './config';

const db = mongoose.createConnection(config.mongo_url);

db.on('error', (err) => {
  console.error('MongoDB connection error: ' + err);
  process.exit(-1);
});

export default db;

