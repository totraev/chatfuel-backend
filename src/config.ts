const { PORT, MONGO_URL } = process.env;

export default {
  port: parseInt(PORT, 10) || 3000,
  mongo_url: MONGO_URL || 'mongodb://localhost:27017/users',
};
