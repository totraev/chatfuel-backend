import * as mongoose from 'mongoose';
import db from '../db';
import * as mongoosePaginate from 'mongoose-paginate';

/**
 * Types
 */
interface Document extends mongoose.Document {
  name: string;
  avatarUrl: string;
}
type PaginationModel = mongoose.PaginateModel<Document>;


/**
 * Model schema
 */
const { ObjectId } = mongoose.Schema.Types;

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    index: true
  },
  avatarUrl: {
    type: String,
    required: true,
  },
});

UserSchema.plugin(mongoosePaginate);

UserSchema.set('toJSON', {
  virtuals: true,
  versionKey: false,
  transform: (_, ret) => { delete ret._id; }
});


export default db.model<Document, PaginationModel>('User', UserSchema);
