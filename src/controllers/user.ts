import User from '../models/user';
import UserService from '../services/user';
import { Handler } from 'express';

/**
 * Show all users
 */
export const index: Handler = async (req, res, next) => {
  const service = new UserService(req.query);

  try {
    const result = await service.findUsers();
    res.status(200).json(result);
  } catch (e) {
    next(e);
  }
};

/**
 * Return user by id
 */
export const view: Handler  = async (req, res, next) => {
  const { id } = req.params;

  try {
    const result = await User.findById(id);
    res.status(200).json({ result });
  } catch (e) {
    next(e);
  }
};
