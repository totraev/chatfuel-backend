import * as express from 'express';
import * as bodyParser from 'body-parser';
import router from './router';

const app: express.Application = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

/**
 * Routes
 */
app.use('/api', router);

/**
 * Error handler
 */
const errorHandler: express.ErrorRequestHandler = (err, req, res, next) => {
  if (res.headersSent) {
    return next(err);
  }
  
  res.status(500).json({
    status: 500,
    error: err.message
  });
};

app.use(errorHandler);

export default app;
