"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_1 = require("../models/user");
const user_2 = require("../services/user");
/**
 * Show all users
 */
exports.index = async (req, res, next) => {
    const service = new user_2.default(req.query);
    try {
        const result = await service.findUsers();
        res.status(200).json(result);
    }
    catch (e) {
        next(e);
    }
};
/**
 * Return user by id
 */
exports.view = async (req, res, next) => {
    const { id } = req.params;
    try {
        const result = await user_1.default.findById(id);
        res.status(200).json({ result });
    }
    catch (e) {
        next(e);
    }
};
