"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const readline = require("readline");
const Faker = require("faker");
const user_1 = require("./models/user");
const CHUNK_COUNT = 1000;
const CHUNK_SIZE = 1000;
function* insert() {
    for (let i = 1; i <= CHUNK_COUNT; i += 1) {
        const users = Array(CHUNK_SIZE).fill(null).map(() => ({
            name: Faker.name.findName(),
            avatarUrl: Faker.image.avatar()
        }));
        yield { i, users };
    }
}
async function seed() {
    for (const { i, users } of insert()) {
        await user_1.default.insertMany(users);
        readline.cursorTo(process.stdout, 0);
        process.stdout.write(`added ${i * CHUNK_SIZE} users`);
    }
    console.log('\ndone.');
    process.exit();
}
seed();
