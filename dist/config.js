"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const { PORT, MONGO_URL } = process.env;
exports.default = {
    port: parseInt(PORT, 10) || 3000,
    mongo_url: MONGO_URL || 'mongodb://localhost:27017/users',
};
