"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const querystring = require("querystring");
const user_1 = require("../models/user");
const FIRST_PAGE = 1;
const PAGE_LIMIT = 50;
class UserService {
    constructor(params) {
        this.findUsers = async () => {
            const { docs, pages } = await user_1.default.paginate(this.query, this.options);
            this.pages = pages;
            return {
                result: docs,
                nextPageUrl: this.nextPageUrl,
                previousPageUrl: this.previousPageUrl
            };
        };
        const page = parseInt(params.page, 10);
        const limit = parseInt(params.limit, 10);
        this.searchTerm = params.searchTerm;
        this.page = page > 0 ? page : FIRST_PAGE;
        this.limit = limit > 0 ? limit : PAGE_LIMIT;
    }
    get options() {
        return {
            limit: this.limit,
            page: this.page
        };
    }
    get query() {
        return this.searchTerm
            ? { name: new RegExp(`^${this.searchTerm}`, 'i') }
            : {};
    }
    getUrl(page) {
        const query = querystring.stringify({
            page,
            limit: this.limit,
            searchTerm: this.searchTerm
        });
        return `/api/users?${query}`;
    }
    get nextPageUrl() {
        return this.page < this.pages ? this.getUrl(this.page + 1) : undefined;
    }
    get previousPageUrl() {
        return this.page > 1 ? this.getUrl(this.page - 1) : undefined;
    }
}
exports.default = UserService;
