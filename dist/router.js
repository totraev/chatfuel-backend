"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const userController = require("./controllers/user");
const router = express_1.Router();
router
    .get('/users', userController.index)
    .get('/users/:id', userController.view);
exports.default = router;
