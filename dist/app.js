"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const router_1 = require("./router");
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
/**
 * Routes
 */
app.use('/api', router_1.default);
/**
 * Error handler
 */
const errorHandler = (err, req, res, next) => {
    if (res.headersSent) {
        return next(err);
    }
    res.status(500).json({
        status: 500,
        error: err.message
    });
};
app.use(errorHandler);
exports.default = app;
