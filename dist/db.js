"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const config_1 = require("./config");
const db = mongoose.createConnection(config_1.default.mongo_url);
db.on('error', (err) => {
    console.error('MongoDB connection error: ' + err);
    process.exit(-1);
});
exports.default = db;
