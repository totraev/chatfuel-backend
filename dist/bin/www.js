"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("../app");
const http = require("http");
const config_1 = require("../config");
/**
 * Get port from environment and store in Express.
 */
app_1.default.set('port', config_1.default.port);
/**
 * Create HTTP server.
 */
const server = http.createServer(app_1.default);
/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(config_1.default.port, () => console.log(`app is running on port ${config_1.default.port}`));
