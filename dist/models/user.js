"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const db_1 = require("../db");
const mongoosePaginate = require("mongoose-paginate");
/**
 * Model schema
 */
const { ObjectId } = mongoose.Schema.Types;
const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        index: true
    },
    avatarUrl: {
        type: String,
        required: true,
    },
});
UserSchema.plugin(mongoosePaginate);
UserSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: (_, ret) => { delete ret._id; }
});
exports.default = db_1.default.model('User', UserSchema);
